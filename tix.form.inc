<?php

/**
 * Implements hook_form();
 * Мастер форма.
 * В зависимости от stage отображаем нужную форму.
 */

function tix_form($form, &$form_state) {
  $libraries = libraries_get_libraries();
  if (empty($libraries['phpexcel'])) {
	drupal_set_message(
	  'Connect the library PHPExcel! ' .
	  l('Download library.', 'https://phpexcel.codeplex.com/releases/view/119187'),
	  'warning'
	);
	drupal_goto('<front>');
  }

  if (!isset($form_state['stage'])) {
	$form_state['stage'] = 'step_1';
  }

  $form = array();

  switch ($form_state['stage']) {
	case 'step_1':
	  return tix_step_1_form($form, $form_state);
	  break;

	case 'step_2':
	  return tix_step_2_form($form, $form_state);
	  break;
  }

  return $form;
}

/**
 * Implements hook_form();
 * Первая форма.
 */
function tix_step_1_form($form, &$form_state) {
  $form = array();
  $vocabularies = array();

  // Извлекаем список всех словарей.
  foreach (taxonomy_vocabulary_get_names() as $row) {
	$vocabularies[$row->vid] = $row->name;
  }

  $form['vocabulary'] = array(
	'#type' => 'select',
	'#title' => t('Clear dictionary:'),
	'#options' => $vocabularies,
	'#default_value' => 0,
  );

  $form['file'] = array(
	'#type' => 'file',
	'#title' => t('File'),
	'#description' => t('A valid file extension: xls.'),
  );

  $form['submit'] = array(
	'#type' => 'submit',
	'#value' => t('Continue'),
  );

  return $form;
}

/**
 * Implements hook_form();
 * Вторая форма.
 */
function tix_step_2_form($form, &$form_state) {
  // При перенаправлении после batch операций на 2ю форму,
  // проверяем завершенность работы и редиректим на страницу всех терминов.
  if (isset($form_state['complete'])) {
	drupal_set_message('Taxonomy terms successfully imported.');
	drupal_goto($form_state['list-import-terms']);
  }

  $form = array();

  // Извлекаем столбцы шапки xls таблицы
  $columsXml = _getColXml($form_state['values']['file']);

  // Извлекаем информацию о словаре
  $vocabulary = taxonomy_vocabulary_load($form_state['values']['vocabulary']);
  $fields = field_info_instances('taxonomy_term', $vocabulary->machine_name);

  $form['vocabulary-text'] = array(
	'#markup' => '<h2>' . t('Vocabulary:') . ' ' . $vocabulary->name . '</h2>',
  );

  $form['tax_elements'] = array('#tree' => TRUE);
  $form['tax_elements_type'] = array('#tree' => TRUE);

  $form['tax_elements']['name'] = array(
	'#type' => 'select',
	'#title' => t('Term name'),
	'#options' => $columsXml,
	'#default_value' => -1,
	'#required' => TRUE,
  );
  $form['tax_elements_type']['name'] = array(
	'#type' => 'hidden',
	'#value' => 'text_textfield',
  );

  $form['tax_elements']['description'] = array(
	'#type' => 'select',
	'#title' => t('Term description'),
	'#options' => $columsXml,
	'#default_value' => -1,
  );
  $form['tax_elements_type']['description'] = array(
	'#type' => 'hidden',
	'#value' => 'text_textfield',
  );

  foreach ($fields as $row) {
	$form['tax_elements'][$row['field_name']] = array(
	  '#type' => 'select',
	  '#title' => t($row['label']),
	  '#options' => $columsXml,
	  '#default_value' => -1,
	);

	$form['tax_elements_type'][$row['field_name']] = array(
	  '#type' => 'hidden',
	  '#value' => $row['widget']['type']
	);

	// Если поле термина обязательно, то отмечаем это на форме
	if ($row['required'] == 1) {
	  $form['tax_elements'][$row['field_name']]['#required'] = TRUE;
	}
  }

  $form['clear-vocabulary'] = array(
	'#type' => 'checkbox',
	'#title' => t('Clear dictionary'),
  );

  $form['back'] = array(
	'#type' => 'submit',
	'#value' => t('Back')
  );

  $form['next'] = array(
	'#type' => 'submit',
	'#value' => t('Import')
  );

  return $form;
}

/**
 * Извлекаем столбцы шапки xls таблицы.
 */
function _getColXml($file) {
  $startRow  = 1; // начальная строка
  $chunkSize = 2; // размер считываемых строк за раз

  $objReader = PHPExcel_IOFactory::createReaderForFile($file->uri);
  $objReader->setReadDataOnly(TRUE); // считываем только текст без форматирования

  $chunkFilter = new chunkReadFilter();
  $objReader->setReadFilter($chunkFilter);

  $chunkFilter->setRows($startRow, $chunkSize);   // устанавливаем знаечение фильтра
  $objPHPExcel = $objReader->load($file->uri);    // открываем файл
  $objPHPExcel->setActiveSheetIndex(0);           // устанавливаем индекс активной страницы
  $objWorksheet = $objPHPExcel->getActiveSheet(); // делаем активной нужную страницу

  // определяем количество столбцов
  $colNumber = PHPExcel_Cell::columnIndexFromString($objWorksheet->getHighestColumn());

  $columns = array(NULL => '- Нет -');
  for ($i = 0; $i < $colNumber; $i++) {
	$columns[$i] = $objWorksheet->getCellByColumnAndRow($i, $startRow)->getValue();
  }

  $objPHPExcel->disconnectWorksheets();
  unset($objPHPExcel); // чистим память

  return $columns;
}

/**
 * Определяем валидатор для текущей формы.
 */
function tix_form_validate($form, &$form_state) {
  switch ($form_state['stage']) {
	case 'step_1':
	  return tix_step_1_validate($form, $form_state);
	  break;
  }
}

/**
 * Проверяем, что загруженный файл в xls формате.
 */
function tix_step_1_validate($form, &$form_state) {
  $file = file_save_upload('file', array('file_validate_extensions' => array('xls')));
  if ($file) {
	$form_state['values']['file'] = $file;
  }
  else {
	form_set_error("file", t("File requred."));
  }
}

/**
 * Определяем обработчик для текущей формы.
 */
function tix_form_submit($form, &$form_state) {
  switch ($form_state['stage']) {
	case 'step_1':
	  // Сохраняем значения формы в multistep_values
	  $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
	  // Присваиваем шаг мастер формы
	  $form_state['stage'] = 'step_2';
	  break;

	case 'step_2':
	  $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
	  // Если была нажата кнопка "назад"
	  if ($form_state['triggering_element']['#value'] == 'Back') {
		$form_state['stage'] = 'step_1';
	  }
	  else {
		$form_state['complete'] = TRUE;

		// Указываем путь на страницу всех терминов по текущему словарю
		$vocabulary = taxonomy_vocabulary_load($form_state['multistep_values']['step_1']['vocabulary']);
		$form_state['list-import-terms'] = '/admin/structure/taxonomy/' . $vocabulary->machine_name;

		tix_step_2_submit($form, $form_state);
	  }
	  break;
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Импортируем термины через batch операции.
 */
function tix_step_2_submit($form, &$form_state) {
  $elements = $form_state['multistep_values']['step_2']['tax_elements'];
  $elements_type = $form_state['multistep_values']['step_2']['tax_elements_type'];
  $vocabulary = $form_state['multistep_values']['step_1']['vocabulary'];

  $operations_del_term = array();
  $operations_import_term = array();

  // Удаляем все термины в словаре, если это было отмечено в чекбоксе
  if ($form_state['multistep_values']['step_2']['clear-vocabulary'] == 1) {
	$tree = taxonomy_get_tree($form_state['multistep_values']['step_1']['vocabulary']);

	foreach ($tree as $term) {
	  $operations_del_term[] = array('tix_delete_term', array($term->tid));
	}
	// Пакетные операция для удаления терминов таксономии.
	$batch = array(
	  'operations' => $operations_del_term,
	  'finished' => 'tix_import_term_finished',
	  'init_message' => 'Подготовка данных',
	  'progress_message' => 'Удалено @current из @total.',
	  'error_message' => 'Произошла ошибка.',
	  'file' => drupal_get_path('module', 'tix') . '/tix_batch.inc',
	);

	batch_set($batch);

  }

  $file =  $form_state['multistep_values']['step_1']['file']->uri;
  $chunkSize = 500;	 // размер считываемых строк за раз
  $startRow  = 2;	 // начинаем читать со строки 2
  $exit = false;	 // флаг выхода

  $vocab = taxonomy_vocabulary_load($vocabulary);
  $vocab_name = $vocab->machine_name;

  $objReader = PHPExcel_IOFactory::createReaderForFile($file);
  $objReader->setReadDataOnly(true);

  $chunkFilter = new chunkReadFilter();
  $objReader->setReadFilter($chunkFilter);

  // внешний цикл, пока файл не кончится
  while (!$exit) {
	$chunkFilter->setRows($startRow,$chunkSize); 	// устанавливаем значение фильтра
	$objPHPExcel = $objReader->load($file); 	    // открываем файл
	$objPHPExcel->setActiveSheetIndex(0);		    // устанавливаем индекс активной страницы
	$objWorksheet = $objPHPExcel->getActiveSheet();	// делаем активной нужную страницу

	// внутренний цикл по строкам
	for ($i = $startRow; $i < $startRow + $chunkSize; $i++) {
	  // получаем первое знаение в строке
	  $value = $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();

	  // проверяем значение на пустоту
	  if (empty($value)) {
		$exit = true;
		break;
	  }

	  // формируем термин
	  $term = new stdClass();
	  $term->vid = $vocabulary;

	  foreach ($elements as $key => $value) {
		if ($key == 'name' || $key == 'description') {
		  $term->{$key} = ($value == NULL) ? '' : $objWorksheet->getCellByColumnAndRow($value, $i)->getValue();
		}
		else {
		  switch($elements_type[$key]) {
			case 'text_textfield':
			  $empty = '';
			  break;

			case 'number':
			  $empty = NULL;
			  break;

			default:
			  $empty = '';
			  break;
		  }

		  $term->{$key}['und'][0]['value'] = ($value == NULL) ? $empty : $objWorksheet->getCellByColumnAndRow($value, $i)->getValue();

		}
	  }

	  $operations_import_term[] = array(
		'tix_import_term',
		array($term, $vocab_name)
	  );
	}

	// чистим память
	$objPHPExcel->disconnectWorksheets();
	unset($objPHPExcel);

	// переходим на следующий шаг цикла, увеличивая строку, с которой будем читать файл
	$startRow += $chunkSize;
  }

  // Пакетные операция для импорта терминов таксономии.
  $batch = array(
	'operations' => $operations_import_term,
	'finished' => 'tix_import_term_finished',
	'init_message' => 'Подготовка данных',
	'progress_message' => 'Импортировано @current из @total.',
	'error_message' => 'Произошла ошибка.',
	'file' => drupal_get_path('module', 'tix') . '/tix_batch.inc',
  );

  batch_set($batch);

}

// Подключаем библиотеки для работы с xls файлом
$libraries = libraries_get_libraries();

if (isset($libraries['phpexcel'])) {
  $path = libraries_get_path('phpexcel');
  require_once $path . '/PHPExcel.php';
  require_once $path . '/PHPExcel/IOFactory.php';
  require_once $path . '/PHPExcel/Reader/IReadFilter.php';

  class chunkReadFilter implements PHPExcel_Reader_IReadFilter {
	private $_startRow = 0;
	private $_endRow = 0;

	/**  Set the list of rows that we want to read  */
	public function setRows($startRow, $chunkSize) {
	  $this->_startRow = $startRow;
	  $this->_endRow = $startRow + $chunkSize;
	}

	public function readCell($column, $row, $worksheetName = '') {
	  //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
	  if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
		return TRUE;
	  }
	  return FALSE;
	}
  }
}