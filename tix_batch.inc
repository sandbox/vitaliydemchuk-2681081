<?php

/**
 * Функция удаления термина
 */
function tix_delete_term($tid, &$context) {
  taxonomy_term_delete($tid);
}

/**
 * Функция добавления или обновления термина
 */
function tix_import_term($term, $vocabulary, &$context) {
  // Ecли термин существует, то обновляем его
  $term_tmp = taxonomy_get_term_by_name($term->name, $vocabulary);
  $term_tmp = array_shift($term_tmp);

  if(!empty($term_tmp->tid)) {
	$term->tid = $term_tmp->tid;
  }

  taxonomy_term_save($term);
}

/**
 * Завершающая функция пакетных операций
 */
function tix_import_term_finished($success, $results, $operations) {
  if ($success) {
	cache_clear_all();
  }
  else {
	drupal_set_message('Завершено с ошибками.', 'error');
  }
}